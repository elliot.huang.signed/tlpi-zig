//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//! Listing 6-1 */                                                          !
//! mem_segments.zig                                                        !
//!                                                                         !
//! A program that does nothing in particular, but the comments indicate    !
//! which memory segments each type of variable is allocated in.            !
//!                                                                         !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const os = std.os;

const globBuf: [65536]u8 = undefined; // Uninitialized data segment
const primes = [_]u8{ 2, 3, 5, 7 }; // Initialized data segment

pub fn square(x: usize) usize { // Allocated in frame for square()
    var result: usize = undefined; // Allocated in frame for square()
    result = x * x;
    return result; // Return value passed via register
}

pub fn doCalc(val: usize) void { // Allocated in frame for doCalc()
    std.debug.print("The square of {d} is {d}\n", .{ val, square(val) });

    if (val < 1000) {
        var t: usize = undefined; // Allocated in frame for doCalc()

        t = val * val * val;
        std.debug.print("The cube of {d} is {d}\n", .{ val, t });
    }
}

pub fn main() !void { // Allocated in frame for main()
    const allocator = std.heap.page_allocator;
    const static = struct {
        var key: usize = 9973; // Initialized data segment
        var mbuf: [10240000]u8 = undefined; // Uninitialized data segment
    };

    var mem: []u8 = undefined; // Allocated in frame for main()

    mem = try allocator.alloc(u8, 1024); // mem.ptr Points to memory in heap segment
    defer allocator.free(mem);

    doCalc(static.key);
}
