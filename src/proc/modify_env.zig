//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//! Listing 6-4                                                             !
//!                                                                         !
//! modify_env.zig                                                          !
//!                                                                         !
//! Demonstrate modification of the process environment list.               !
//!                                                                         !
//! Usage: modify_env name=value...                                         !
//!                                                                         !
//! Note: some UNIX implementations do not provide clearenv(), setenv(),    !
//! and unsetenv().                                                         !
//!                                                                         !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const tlpi = @import("tlpi");
const process = std.process;
const os = std.os;
const print = std.debug.print;

pub fn main() !void {
    const argv = os.argv;
    const allocator = std.heap.page_allocator;
    var envMap = try process.getEnvMap(allocator);

    tlpi.clearenv(&envMap); // Erase entire environment

    var iter = process.EnvMap.iterator(&envMap);

    for (argv[1..]) |env| {
        try tlpi.putenv(&envMap, std.mem.span(env));
    }

    try tlpi.setenv(&envMap, "GREET", "Hello world");

    tlpi.unsetenv(&envMap, "BYE");

    while (iter.next()) |entry| {
        print("{s}={s}\n", .{ entry.key_ptr.*, entry.value_ptr.* });
    }
}
