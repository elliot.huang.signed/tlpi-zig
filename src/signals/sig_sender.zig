const std = @import("std");
const mem = std.mem;
const fmt = std.fmt;
const os = std.os;
const print = std.debug.print;
const errno = os.system.getErrno;

pub fn main() !void {
    const argv = os.argv;
    if (argv.len < 4 or mem.eql(u8, mem.span(argv[1]), "--help")) {
        print("{s} pid num-sigs sig-num [sig-num-2]\n", .{argv[0]});
        return;
    }

    var pid = try fmt.parseInt(os.pid_t, mem.span(argv[1]), 0);
    var numSigs = try fmt.parseInt(usize, mem.span(argv[2]), 0);
    var sig = try fmt.parseInt(u8, mem.span(argv[3]), 0);

    print("{s} sending signal {d} to process {d} {d} times\n", .{ argv[0], sig, pid, numSigs });

    var j: usize = 0;
    while (j < numSigs) : (j += 1) {
        try os.kill(pid, sig);
    }

    if (argv.len > 4) {
        sig = try fmt.parseInt(u8, mem.span(argv[4]), 0);
        try os.kill(pid, sig);
    }
    print("{s}: exiting\n", .{argv[0]});
}
