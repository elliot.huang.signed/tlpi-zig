const std = @import("std");
const os = std.os;
const print = std.debug.print;

fn pause() usize {
    return os.system.syscall0(.pause);
}

fn sigHandler(sig: c_int) callconv(.C) void {
    const s = struct {
        var count: usize = 0;
    };

    if (sig == os.SIG.INT) {
        s.count += 1;
        print("Caught SIGINT ({d})\n", .{s.count});
        return;
    }

    print("Caught SIGQUIT - that's all folks!\n", .{});
    os.exit(0);
}

pub fn main() !void {
    const sigact = os.Sigaction{
        .handler = .{ .handler = &sigHandler },
        .mask = os.empty_sigset,
        .flags = 0,
    };
    try os.sigaction(os.SIG.INT, &sigact, null);
    try os.sigaction(os.SIG.QUIT, &sigact, null);

    while (true) {
        _ = pause();
    }
}
