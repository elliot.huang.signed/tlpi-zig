const std = @import("std");
const mem = std.mem;
const fmt = std.fmt;
const os = std.os;
const print = std.debug.print;
const errno = os.system.getErrno;

pub fn main() !void {
    const argv = os.argv;
    if (argv.len != 3 or mem.eql(u8, mem.span(argv[1]), "--help")) {
        print("{s} pid sig-num\n", .{argv[0]});
        return;
    }

    var sig = try fmt.parseInt(u8, mem.span(argv[2]), 0);
    var pid = try fmt.parseInt(os.pid_t, mem.span(argv[1]), 0);

    switch (errno(os.system.kill(pid, sig))) {
        .SUCCESS => {
            print("Process exists and we can send it a signal\n", .{});
            return;
        },
        .INVAL => unreachable, // invalid signal
        .PERM => return error.PermissionDenied,
        .SRCH => {
            print("Process does not exist\n", .{});
        },
        else => |err| return os.unexpectedErrno(err),
    }
}
