const std = @import("std");
const sigfunc = @import("signal_functions.zig");
const mem = std.mem;
const fmt = std.fmt;
const os = std.os;
const print = std.debug.print;
const errno = os.system.getErrno;

var sigCnt: [os.system.NSIG]usize = undefined;
var gotSigint: bool = false;

pub fn sigpending(set: *os.sigset_t) void {
    const NSIG = os.system.NSIG;
    _ = os.system.syscall2(.rt_sigpending, @ptrToInt(set), NSIG / 8);
}

fn handler(sig: c_int) callconv(.C) void {
    if (sig == os.SIG.INT) {
        gotSigint = true;
    } else {
        sigCnt[@intCast(usize, sig)] += 1;
    }
}

pub fn main() !void {
    const argv = os.argv;
    const stdout = std.io.getStdOut();
    var pendingMask: os.sigset_t = undefined;
    print("{s}: PID is {d}\n", .{ argv[0], os.system.getpid() });

    var n: u6 = 1;
    while (n < os.system.NSIG - 2) : (n += 1) {
        if (!(n == os.SIG.KILL or n == os.SIG.STOP)) {
            const sigact = os.Sigaction{
                .handler = .{ .handler = &handler },
                .mask = os.empty_sigset,
                .flags = 0,
            };

            _ = try os.sigaction(n, &sigact, null);
        }
    }

    if (argv.len > 1) {
        var numSecs = try fmt.parseInt(usize, mem.span(argv[1]), 0);
        os.sigprocmask(os.SIG.SETMASK, &os.system.all_mask, null);

        print("{s}: sleeping for {d} seconds\n", .{ argv[0], numSecs });
        std.time.sleep(numSecs * std.time.ns_per_s);
        sigpending(&pendingMask);

        print("{s}: pending signals are: \n", .{argv[0]});
        try sigfunc.printSigSet(stdout, "\t\t", &pendingMask);
        os.sigprocmask(os.SIG.SETMASK, &os.empty_sigset, null);
    }

    while (!gotSigint) {
        continue;
    }

    n = 1;
    while (n < os.system.NSIG - 2) : (n += 1) {
        if (sigCnt[@intCast(usize, n)] != 0) {
            print(
                "{s}: signal {d} caught {d} time{s}\n",
                .{
                    argv[0],
                    n,
                    sigCnt[@intCast(usize, n)],
                    if (sigCnt[@intCast(usize, n)] == 1) "" else "s",
                },
            );
        }
    }
}
