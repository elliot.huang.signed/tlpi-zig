const std = @import("std");
const os = std.os;
const fmt = std.fmt;
const print = std.debug.print;

pub fn printSigSet(file: std.fs.File, prefix: []const u8, sigset: *os.sigset_t) !void {
    const allocator = std.heap.page_allocator;
    var cnt: usize = 0;
    var sig: u6 = 1;

    while (sig < os.system.NSIG - 2) : (sig += 1) {
        if (os.system.sigismember(sigset, sig)) {
            cnt += 1;
            const out = try fmt.allocPrint(
                allocator,
                "{s}{d}\n",
                .{ prefix, sig },
            );
            _ = try file.writer().write(out);
        }
    }

    if (cnt == 0) {
        const out = try fmt.allocPrint(
            allocator,
            "{s}<empty signal set>\n",
            .{prefix},
        );
        _ = try file.writer().write(out);
    }
}
