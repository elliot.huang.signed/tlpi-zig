const std = @import("std");
const os = std.os;
const print = std.debug.print;

fn sigHandler(sig: c_int) callconv(.C) void {
    _ = sig; // make zig compiler happy
    print("Ouch!\n", .{});
}

pub fn main() !void {
    var j: usize = 0;
    const sigact = os.Sigaction{
        .handler = .{ .handler = &sigHandler },
        .mask = os.empty_sigset,
        .flags = 0,
    };
    try os.sigaction(os.SIG.INT, &sigact, null);

    // Loop continuously waiting for signals to be delivered
    while (true) : (j += 1) {
        print("{d}\n", .{j});
        std.time.sleep(3 * std.time.ns_per_s);
    }
}
