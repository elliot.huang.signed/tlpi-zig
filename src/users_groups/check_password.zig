//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//! Listing 8-2                                                             !
//!                                                                         !
//! check_password.zig                                                      !
//!                                                                         !
//! Read a user name and password and check if they are valid.              !
//!                                                                         !
//! This program uses the shadow password file. Some UNIX implementations   !
//! don't support this feature.                                             !
//!                                                                         !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const tlpi = @import("tlpi");
const os = std.os;
const print = std.debug.print;

pub fn main() !void {
    // ToDo: get system supported max user name
    const stdin = std.io.getStdIn().reader();
    var buf: [128]u8 = undefined;

    print("Username: ", .{});
    var username = (try stdin.readUntilDelimiterOrEof(&buf, '\n')).?;

    var pwd = try tlpi.getpwnam(username);
    var spwd = try tlpi.getspnam(username);
    pwd.pw_passwd = spwd.sp_pwdp;
    var salt = pwd.pw_passwd[0..20].*;
    var password = try tlpi.getpass("Password: ");

    // Encrypt password and erase cleartext version immediately
    var encrypted = try tlpi.crypt(password, &salt);
    for (password) |*pw| {
        pw.* = '0';
    }

    var authOk = std.mem.eql(u8, encrypted, pwd.pw_passwd);
    if (!authOk) {
        print("Incorrect password\n", .{});
        return;
    }

    print("Successfully authenticated: UID={d}\n", .{pwd.pw_uid});
    // Now do authenticated work...
}
