//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//! Listing 12-2                                                            !
//!                                                                         !
//! t_uname.zig                                                             !
//!                                                                         !
//! Demonstrate the use of the uname() system call, which returns various   !
//! identifying information about the system.                               !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const os = std.os;
const print = std.debug.print;

pub fn main() !void {
    var uts = os.uname();

    print("Node name:   {s}\n", .{uts.nodename});
    print("System name: {s}\n", .{uts.sysname});
    print("Release:     {s}\n", .{uts.release});
    print("Version:     {s}\n", .{uts.version});
    print("Machine:     {s}\n", .{uts.machine});
    print("Domain name: {s}\n", .{uts.domainname});
}
