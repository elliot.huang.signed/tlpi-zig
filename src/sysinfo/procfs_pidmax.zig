//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//! Listing 12-1                                                            !
//!                                                                         !
//! procfs_pidmax.zig                                                       !
//!                                                                         !
//! This program demonstrates how to access a file in the /proc file system.!
//! It can be used to read and modify the /proc/sys/kernel/pid_max file     !
//! (which is available only in Linux 2.6 and later).                       !
//!                                                                         !
//! Usage: procfs_pidmax [new-pidmax]                                       !
//!                                                                         !
//!     Displays the current maximum PID, and, if a command line            !
//!     argument is supplied, sets the maximum PID to that value.           !
//!                                                                         !
//!     Note: privilege is required to change the maximum PID value.        !
//!     This program is Linux-specific.                                     !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const os = std.os;
const print = std.debug.print;

const MAX_LINE = 100;

pub fn main() !void {
    const argv = os.argv;
    var line: [MAX_LINE]u8 = undefined;
    const fd = try os.open("/proc/sys/kernel/pid_max", if (argv.len > 1) os.O.RDWR else os.O.RDONLY, 0);
    defer os.close(fd);
    const n = try os.read(fd, &line);

    if (argv.len > 1) {
        print("Old value: {s}\n", .{line[0..n]});
    }

    if (argv.len > 1) {
        try os.lseek_SET(fd, 0);
        _ = try os.write(fd, std.mem.span(argv[1]));
        try os.fsync(fd); // need to do fsync before closing fd
        const allocator = std.heap.page_allocator;
        var cmd = .{ "cat", "/proc/sys/kernel/pid_max" };
        var result = try std.ChildProcess.exec(.{ .argv = &cmd, .allocator = allocator });
        print("/proc/sys/kernel/pid_max now contains {s}\n", .{result.stdout});
    }
}
