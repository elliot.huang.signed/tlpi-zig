//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!
//!  Listing 4-1
//!
//!  copy.zig
//!
//!  Copy the file named argv[1] to a new file named in argv[2].
//!

const std = @import("std");
const os = std.os;
const BUF_SIZE = 1024;

pub fn main() !void {
    const argv = os.argv;
    var rbuf: [BUF_SIZE]u8 = undefined;
    var numRead: usize = undefined;
    var numWrite: usize = undefined;

    if (argv.len != 3 or std.mem.eql(u8, std.mem.span(argv[1]), "--help")) {
        std.debug.print("{s} old-file new-file\n", .{argv[0]});
        return;
    }

    // Open input and output files
    const inputFd = try os.open(std.mem.span(argv[1]), os.O.RDONLY, 0);
    defer os.close(inputFd);

    const openFlags = os.O.CREAT | os.O.WRONLY | os.O.TRUNC;
    const filePerms = os.S.IRUSR | os.S.IWUSR | os.S.IRGRP | os.S.IWGRP | os.S.IROTH | os.S.IWOTH; // rw-rw-rw-
    const outputFd = try os.open(std.mem.span(argv[2]), openFlags, filePerms);
    defer os.close(outputFd);

    // Transfer data until we encounter end of input or an error
    while (true) {
        numRead = try os.read(inputFd, std.mem.span(&rbuf));
        var wBuf = rbuf[0..numRead];
        if (numRead > 0) {
            numWrite = try os.write(outputFd, wBuf);
        } else {
            break;
        }
    }
    try os.fsync(outputFd);
}
