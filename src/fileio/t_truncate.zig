//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                                                                         !
//!  Supplementary program for Chapter 5                                    !
//!                                                                         !
//!  t_truncate.zig                                                         !
//!                                                                         !
//!  Demonstrate the use of the truncate() system call to truncate the file !
//!  named in argv[1] to the length specified in argv[2]                    !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const tlpi = @import("tlpi");
const os = std.os;
const mem = std.mem;
const fmt = std.fmt;
const print = std.debug.print;

pub fn main() !void {
    const argv = os.argv;
    if (argv.len != 3 or mem.eql(u8, mem.span(argv[1]), "--help")) {
        print("{s} file length\n", .{argv[0]});
        return;
    }
    const length = try fmt.parseInt(u64, mem.span(argv[2]), 0);
    try tlpi.truncateZ(mem.span(argv[1]), length);
}
