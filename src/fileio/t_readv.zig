//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                   Copyright (C) BinaryCraft, 2022.                       !
//!                                                                          !
//!  This program is free software. You may use, modify, and redistribute it !
//!  under the terms of the GNU General Public License as published by the   !
//!  Free Software Foundation, either version 3 or (at your option) any      !
//!  later version. This program is distributed without any warranty.  See   !
//!  the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!
//! Listing 5-2
//! t_readv.zig
//! Demonstrate the use of the readv() system call to perform "gather I/O".
//! (This program is merely intended to provide a code snippet for the book;
//! unless you construct a suitably formatted input file, it can't be
//! usefully executed.)
//!

const std = @import("std");
const os = std.os;
const STR_SIZE = 100;

pub fn main() !void {
    const argv = os.argv;
    var iov: [3]os.iovec = undefined;
    var myStruct: os.Stat = undefined;
    var x: u8 = undefined;
    var str: [STR_SIZE]u8 = undefined;
    var totRequired: usize = 0;

    if (argv.len != 2 or std.mem.eql(u8, std.mem.span(argv[1]), "--help")) {
        std.debug.print("{s} file\n", .{argv[0]});
        return;
    }

    const fd = try os.open(std.mem.span(argv[1]), os.O.RDONLY, 0);
    defer os.close(fd);

    iov[0] = .{
        .iov_base = @ptrCast([*]u8, &myStruct),
        .iov_len = @sizeOf(@TypeOf(myStruct)),
    };
    totRequired += iov[0].iov_len;

    iov[1] = .{
        .iov_base = @ptrCast([*]u8, &x),
        .iov_len = @sizeOf(@TypeOf(x)),
    };
    totRequired += iov[1].iov_len;

    iov[2] = .{
        .iov_base = &str,
        .iov_len = STR_SIZE,
    };
    totRequired += iov[2].iov_len;

    var numRead = try os.readv(fd, std.mem.span(&iov));
    if (numRead < totRequired) {
        std.debug.print("Read fewer bytes than requested\n", .{});
    }

    std.debug.print("total bytes requested: {d}; bytes read: {d}\n", .{ totRequired, numRead });
}
