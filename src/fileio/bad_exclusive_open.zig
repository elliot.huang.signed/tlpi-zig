//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//! Listing 5-1
//! bad_exclusive_open.zig
//! The following code shows why we need the open() O_EXCL flag.
//! This program to tries ensure that it is the one that creates the file
//! named in its command-line argument. It does this by trying to open()
//! the filename once without the O_CREAT flag (if this open() succeeds
//! then the program know it is not the creator of the file), and if
//! that open() fails, it calls open() a second time, with the O_CREAT flag.
//! If the first open() fails, the program assumes that it is the creator
//! of the file. However this may not be true: some other process may have
//! created the file between the two calls to open().

const std = @import("std");
const os = std.os;

pub fn main() !void {
    const argv = os.argv;

    if (argv.len < 2 or std.mem.eql(u8, std.mem.span(argv[1]), "--help")) {
        std.debug.print("{s} file\n", .{argv[0]});
    }

    if (os.open(std.mem.span(argv[1]), os.O.WRONLY, 0)) |fd| {
        defer os.close(fd);
        std.debug.print("[PID {d}] File \"{s}\" already exists\n", .{ os.system.getpid(), argv[1] });
    } else |err| switch (err) {
        error.FileNotFound => {
            std.debug.print("[PID {d}] File \"{s}\" doesn't exist yet\n", .{ os.system.getpid(), argv[1] });
            if (argv.len > 2) { // Delay between check and create
                std.time.sleep(5 * 1000000000); // Suspend execution for 5 seconds
                std.debug.print("[PID {d}] Done sleeping\n", .{os.system.getpid()});
            }

            var fd = try os.open(std.mem.span(argv[1]), os.O.WRONLY |
                os.O.CREAT, os.S.IRUSR | os.S.IWUSR);
            defer os.close(fd);
            std.debug.print("[PID {d}] Created file \"{s}\" exclusively\n", .{ os.system.getpid(), argv[1] });
        },
        else => {
            return err;
        },
    }
}
