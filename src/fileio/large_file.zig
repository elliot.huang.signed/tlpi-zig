//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                                                                         !
//! Listing 5-3                                                             !
//!                                                                         !
//! large_file.zig                                                          !
//!                                                                         !
//! Large File System API is obsolete, nothing needs to be done for         !
//! supporting large file in zig. (Not using libc).                         !
//!                                                                         !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const os = std.os;
const mem = std.mem;
const fmt = std.fmt;
const print = std.debug.print;

pub fn main() !void {
    const argv = os.argv;
    if (argv.len != 3 or mem.eql(u8, mem.span(argv[1]), "--help")) {
        print("{s} pathname offset\n", .{argv[0]});
        return;
    }

    const fd = try os.open(mem.span(argv[1]), os.O.RDWR | os.O.CREAT, os.S.IRUSR | os.S.IWUSR);
    defer os.close(fd);

    const off = try fmt.parseInt(u64, mem.span(argv[2]), 0);
    try os.lseek_SET(fd, off);
    _ = try os.write(fd, "test");
    try os.fsync(fd);
}
