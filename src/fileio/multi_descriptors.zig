//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                                                                         !
//! Solution for Exercise 5-6                                               !
//!                                                                         !
//! multi_descriptors.zig                                                   !
//!                                                                         !
//! Show the interaction of multiple descriptors accessing the same         !
//! file (some via the same shared open file table entry).                  !
//!                                                                         !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const os = std.os;
const mem = std.mem;
const print = std.debug.print;

pub fn system(cmd: []const []const u8) !void {
    const allocator = std.heap.page_allocator;
    const result = try std.ChildProcess.exec(.{ .argv = cmd, .allocator = allocator });
    print("{s}\n", .{result.stdout});
}

pub fn main() !void {
    const file = "a";
    var cmd = .{ "cat", file };
    const fd1 = try os.open(mem.span(file), os.O.RDWR | os.O.CREAT | os.O.TRUNC, os.S.IRUSR | os.S.IWUSR);
    const fd2 = try os.dup(fd1);
    const fd3 = try os.open(mem.span(file), os.O.RDWR, 0);
    defer {
        os.close(fd1);
        os.close(fd2);
        os.close(fd3);
    }

    // 'fd1' and 'fd2' share same open file table entry (and thus file
    // offset). 'fd3' has its own open file table entry, and thus a
    // separate file offset.

    _ = try os.write(fd1, "hello");
    try os.fsync(fd1);
    try system(&cmd);

    _ = try os.write(fd2, "world");
    try os.fsync(fd2);
    try system(&cmd);

    try os.lseek_SET(fd2, 0);
    _ = try os.write(fd1, "HELLO");
    try os.fsync(fd1);
    try system(&cmd);

    _ = try os.write(fd3, "Gidday");
    try os.fsync(fd3);
    try system(&cmd);
}
