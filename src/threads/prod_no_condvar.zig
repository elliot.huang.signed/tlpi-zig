const std = @import("std");
const Thread = std.Thread;
const os = std.os;
const mem = std.mem;
const fmt = std.fmt;
const time = std.time;
const print = std.debug.print;

var avail: usize = 0;
var mutex = Thread.Mutex{};

pub fn main() !void {
    const start = try time.Instant.now();
    const argv = os.argv;
    var totRequired: usize = 0;

    for (argv[1..]) |arg| {
        var cnt = try fmt.parseInt(usize, mem.span(arg), 0);
        totRequired += cnt;
        _ = try Thread.spawn(.{}, producer, .{&cnt});
    }

    var numConsumed: usize = 0;
    var done: bool = false;

    while (true) {
        mutex.lock();
        defer mutex.unlock();

        while (avail > 0) {
            numConsumed += 1;
            avail -= 1;

            const now = try time.Instant.now();
            print(
                "T={d}: numConsumed={d}\n",
                .{ time.Instant.since(now, start), numConsumed },
            );
            done = numConsumed >= totRequired;
        }

        if (done) {
            break;
        }
    }
}

fn producer(arg: *const usize) void {
    var j: usize = 0;
    while (j < arg.*) : (j += 1) {
        time.sleep(1 * 1000000000);

        mutex.lock();
        defer mutex.unlock();

        avail += 1;
    }
}
