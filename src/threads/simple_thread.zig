const std = @import("std");
const Thread = std.Thread;
const mem = std.mem;
const print = std.debug.print;

pub fn main() !void {
    const allocator = std.heap.page_allocator;
    var s: []u8 = try allocator.dupe(u8, "Hello world\n");

    const thread = try Thread.spawn(.{}, threadFunc, .{&s});
    print("Message from main()\n", .{});
    thread.join();
    print("Thread returned\n", .{});
}

fn threadFunc(arg: *const []u8) void {
    print("{s}", .{arg.*});
}
