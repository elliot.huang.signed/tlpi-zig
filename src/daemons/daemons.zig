const std = @import("std");
const os = std.os;
const errno = os.system.getErrno;

const BD_NO_CHDIR = 1;
const BD_NO_CLOSE_FILES = 2;
const BD_NO_REOPEN_STD_FDS = 4;
const BD_NO_UMASK0 = 10;
const BD_MAX_CLOSE = 8192;

pub fn setsid() !os.pid_t {
    const rc = os.system.syscall0(.setsid);
    switch (errno(rc)) {
        .SUCCESS => return @intCast(os.pid_t, rc),
        .PERM => return error.PermissionDenied,
        else => |err| return os.unexpectedErrno(err),
    }
}

pub fn umask(mode: os.mode_t) os.mode_t {
    const res = os.system.syscall1(.umask, mode);
    return res;
}

pub fn becomeDaemon(flags: i16) !void {
    var childPid = try os.fork();
    switch (childPid) {
        0 => {},
        else => {
            os.exit(0);
        },
    }

    _ = try setsid();

    childPid = try os.fork();
    switch (childPid) {
        0 => {},
        else => {
            os.exit(0);
        },
    }

    if ((flags & BD_NO_UMASK0) < 0) {
        _ = umask(0);
    }

    if ((flags & BD_NO_CHDIR) < 0) {
        try os.chdir("/");
    }

    if ((flags & BD_NO_CLOSE_FILES) < 0) {
        const maxfd = BD_MAX_CLOSE;
        var fd: i32 = 0;
        while (fd < maxfd) : (fd += 1) {
            os.close(fd);
        }
    }

    if ((flags & BD_NO_REOPEN_STD_FDS) < 0) {
        os.close(os.STDIN_FILENO);
        _ = try os.open("dev/null", os.O.RDWR, 0);

        try os.dup2(os.STDIN_FILENO, os.STDOUT_FILENO);
        try os.dup2(os.STDIN_FILENO, os.STDERR_FILENO);
    }
}
