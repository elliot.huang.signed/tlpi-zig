//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                                                                         !
//! Listing 59-4                                                            !
//!                                                                         !
//! i6d_ucase_cl.zig                                                        !
//!                                                                         !
//! Client for i6d_ucase_sv.zig: send each command-line argument as a       !
//! datagram to the server, and then display the contents of the server's   !
//! response datagram.                                                      !
//!                                                                         !
//! See also i6d_ucase_sv.zig.                                              !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const os = std.os;
const mem = std.mem;
const fmt = std.fmt;
const net = std.net;
const log = std.log;
const ascii = std.ascii;
const print = std.debug.print;
const system = os.system;

const BUF_SIZE = 10;
const PORT_NUM = 50002;

pub fn main() !void {
    const argv = os.argv;
    var resp: [BUF_SIZE]u8 = undefined;
    // Create a datagram socket; send to an address in the IPv6 domain
    const sockfd = try os.socket( // Create client socket
        os.AF.INET6,
        os.SOCK.DGRAM,
        0,
    );

    if (argv.len < 3 or mem.eql(u8, mem.span(argv[1]), "--help")) {
        print("{s} host-address msg...\n", .{argv[0]});
        return;
    }

    const svaddr = try net.Address.parseIp6(mem.span(argv[1]), PORT_NUM);

    // Send messages to server; echo responses on stdout
    for (argv[2..argv.len]) |arg, j| {
        const data = mem.span(arg);
        const msgLen = svaddr.getOsSockLen();
        var bytesSent = try os.sendto(sockfd, data, 0, &svaddr.any, msgLen);
        if (bytesSent != data.len) {
            log.warn("Failed to send all data", .{});
        }
        var bytesReceived = try os.recvfrom(sockfd, &resp, 0, null, null);
        if (bytesSent != bytesReceived) {
            log.warn("Failed to receive all data", .{});
        }
        print("Response {d}: {s}\n", .{ j + 1, resp[0..bytesReceived] });
    }
}
