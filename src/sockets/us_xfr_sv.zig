//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                                                                         !
//! Listing 57-3                                                            !
//!                                                                         !
//! us_xfr_sv.zig                                                           !
//!                                                                         !
//! An example UNIX stream socket server. Accepts incoming connections      !
//! and copies data sent from clients to stdout.                            !
//!                                                                         !
//! See also us_xfr_cl.zig.                                                 !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const os = std.os;
const io = std.io;
const mem = std.mem;
const net = std.net;
const log = std.log;
const system = os.system;

const SV_SOCK_PATH = "/tmp/us_xfr";
const BUF_SIZE = 100;
const BACKLOG = 5;

pub fn main() !void {
    var buf: [BUF_SIZE]u8 = undefined;
    const stdoutFile = io.getStdOut();

    // Construct server socket address, bind socket to it,
    // and make this a listening socket
    os.unlink(SV_SOCK_PATH) catch |err| switch (err) {
        error.FileNotFound => {},
        else => {
            return err;
        },
    };

    var addr = try std.net.Address.initUnix(SV_SOCK_PATH);
    var server = net.StreamServer.init(net.StreamServer.Options{});
    defer server.deinit();

    try server.listen(addr);
    // Handle client connections iteratively
    while (true) {
        // Accept a connection. The connection is returned on a new
        // socket, 'cfd'; the listening socket ('sfd') remains open
        // and can be used to accept further connections.
        var connection = try server.accept();
        defer connection.stream.close();

        // Transfer data from connected socket to stdout until EOF
        while (true) {
            var numRead = try connection.stream.reader().read(&buf);
            if (numRead <= 0) {
                break;
            }

            var numWrite = try stdoutFile.writer().write(buf[0..numRead]);
            if (numWrite != numRead) {
                log.err("partial/failed write\n", .{});
            }
        }
    }
}
