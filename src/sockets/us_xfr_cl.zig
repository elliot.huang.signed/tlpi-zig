//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                                                                         !
//! Listing 57-4                                                            !
//!                                                                         !
//! us_xfr_cl.zig                                                           !
//!                                                                         !
//! An example UNIX domain stream socket client. This client transmits      !
//! contents and copies data sent from clients to stdout.                   !
//!                                                                         !
//! See also us_xfr_sv.zig.                                                 !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const os = std.os;
const io = std.io;
const mem = std.mem;
const net = std.net;
const log = std.log;
const system = os.system;

const SV_SOCK_PATH = "/tmp/us_xfr";
const BUF_SIZE = 100;

pub fn main() !void {
    const stdinFile = io.getStdIn();
    var buf: [BUF_SIZE]u8 = undefined;

    // Construct server address, and make the connection
    var stream = try net.connectUnixSocket(SV_SOCK_PATH);
    defer stream.close();

    // Copy stdin to socket
    while (true) {
        var numRead = try stdinFile.reader().read(&buf);
        if (numRead <= 0) {
            break;
        }

        var numWrite = try stream.writer().write(buf[0..numRead]);
        if (numWrite != numRead) {
            log.err("partial/failed write\n", .{});
        }
    }
}
