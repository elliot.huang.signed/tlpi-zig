//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                                                                         !
//! Listing 57-7                                                            !
//!                                                                         !
//! ud_ucase_cl.zig                                                         !
//!                                                                         !
//! A UNIX domain client that communicates with the server in               !
//! ud_ucase_sv.zig datagrams, This client sends each command-line          !
//! argument as a datagram to the server, and then return them to           !
//! the sender. and then displays the contents of the server's              !
//! response datagram.                                                      !
//!                                                                         !
//! See also ud_ucase_sv.zig.                                               !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const os = std.os;
const mem = std.mem;
const net = std.net;
const log = std.log;
const ascii = std.ascii;
const print = std.debug.print;
const system = os.system;

const SV_SOCK_PATH = "/tmp/ud_ucase";
const BUF_SIZE = 10;

pub fn main() !void {
    var argv = os.argv;
    var allocator = std.heap.page_allocator;
    var resp: [BUF_SIZE]u8 = undefined;

    // Create client socket
    const sockfd = try os.socket(
        os.AF.UNIX,
        os.SOCK.DGRAM,
        0,
    );

    if (argv.len < 2 or mem.eql(u8, mem.span(argv[1]), "--help")) {
        print("{s} msg...\n", .{argv[0]});
        return;
    }

    // Bind client socket to unique pathname (based on PID)
    const CL_SOCK_PATH = try std.fmt.allocPrint(
        allocator,
        "/tmp/ud_ucase_cl.{d}",
        .{system.getpid()},
    );
    defer allocator.free(CL_SOCK_PATH);

    // Construct address of server and client
    var claddr = try std.net.Address.initUnix(CL_SOCK_PATH);
    var svaddr = try std.net.Address.initUnix(SV_SOCK_PATH);
    try os.bind(sockfd, &claddr.any, claddr.getOsSockLen());

    // Send messages to server; echo responses on stdout
    for (argv[1..argv.len]) |arg, j| {
        var len = svaddr.getOsSockLen();
        var data = mem.span(arg); // May be longer than BUF_SIZE
        var bytesSent = try os.sendto(sockfd, data, 0, &svaddr.any, len);
        if (bytesSent != data.len) {
            log.warn("Failed to send all data", .{});
        }
        var bytesReceived = try os.recvfrom(sockfd, &resp, 0, null, null);
        if (bytesSent != bytesReceived) {
            log.warn("Failed to receive all data", .{});
        }
        print("Response {d}: {s}\n", .{ j + 1, resp[0..bytesReceived] });
    }
    // Remove client socket pathname
    try os.unlink(CL_SOCK_PATH);
}
