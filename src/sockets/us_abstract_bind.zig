//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                                                                         !
//! Listing 57-8                                                            !
//!                                                                         !
//! us_abstract_bind.zig                                                    !
//!                                                                         !
//! Demonstrate how to bind a UNIX domain socket to a name in the           !
//! Linux-specific abstract namespace.                                      !
//!                                                                         !
//! This program is Linux-specific.                                         !
//!                                                                         !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const os = std.os;
const mem = std.mem;
const net = std.net;
const log = std.log;
const system = os.system;

pub fn main() !void {
    var sock_addr = net.Address{
        .un = os.sockaddr.un{
            .family = os.AF.UNIX, // UNIX domain address
            .path = undefined,
        },
    };

    const sockfd = try os.socket(
        os.AF.UNIX,
        os.SOCK.STREAM,
        0,
    );

    var str = "xyz"; // Abstract name is "\0xyz"
    mem.set(u8, &sock_addr.un.path, 0); // Clear address structure
    // sock_addr.un.path[0] has already been set to 0 by mem.set()
    mem.copy(u8, sock_addr.un.path[1 .. str.*.len + 1], str);

    var len = @intCast(os.socklen_t, @sizeOf(os.sa_family_t) + str.*.len + 1);
    try os.bind(sockfd, &sock_addr.any, len);
    std.time.sleep(60 * 1000000000);
}
