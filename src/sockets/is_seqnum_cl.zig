//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                                                                         !
//! Listing 59-7                                                            !
//!                                                                         !
//! is_seqnum_cl.zig                                                        !
//!                                                                         !
//! A simple Internet stream socket client. This client requests a sequence !
//! number from the server.                                                 !
//!                                                                         !
//! See also is_seqnum_sv.zig.                                              !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const builtin = @import("builtin");
const os = std.os;
const mem = std.mem;
const fmt = std.fmt;
const net = std.net;
const log = std.log;
const ascii = std.ascii;
const print = std.debug.print;

const BUF_SIZE = 256;
const PORT_NUM = 50000;

pub const log_level: std.log.Level = .info;

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();

    const argv = try std.process.argsAlloc(gpa.allocator());
    defer std.process.argsFree(gpa.allocator(), argv);

    if (argv.len < 2 or mem.eql(u8, mem.span(argv[1]), "--help")) {
        print("{s} server-host [sequence-len]\n", .{argv[0]});
        return;
    }

    if (builtin.os.tag == .windows) {
        _ = try os.windows.WSAStartup(2, 2);
    }
    defer {
        if (builtin.os.tag == .windows) {
            os.windows.WSACleanup() catch unreachable;
        }
    }

    const connection = try net.tcpConnectToHost(
        gpa.allocator(),
        mem.span(argv[1]),
        PORT_NUM,
    );
    defer connection.close();

    // Send requested sequence length, with terminating newline
    var reqLenStr: []u8 = undefined;
    if (argv.len > 2) {
        reqLenStr = try fmt.allocPrint(
            gpa.allocator(),
            "{s}\n",
            .{argv[2]},
        );
    } else {
        reqLenStr = try fmt.allocPrint(
            gpa.allocator(),
            "1\n",
            .{},
        );
    }
    defer gpa.allocator().free(reqLenStr);

    var numWrite = try connection.writer().write(reqLenStr);
    if (numWrite != reqLenStr.len) {
        log.err("Partial/failed write (reqLenStr)", .{});
        return;
    }

    // Read and display sequence number returned by server
    var buffer: [BUF_SIZE]u8 = undefined;
    const line = try connection.reader().readUntilDelimiterOrEof(
        &buffer,
        '\n',
    );

    if (line) |seqNum| {
        log.info("Sequence number: {s}", .{seqNum});
    } else {
        log.err("Unexpected EOF from server", .{});
    }
}
