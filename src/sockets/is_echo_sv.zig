const std = @import("std");
const daemons = @import("daemons");
const os = std.os;
const net = std.net;
const log = std.log;

const SERVICE = "echo";
const WNOHANG = 1;
const BUF_SIZE = 4096;
const inaddr_any = [4]u8{ 0, 0, 0, 0 };

pub fn grimReaper(sig: c_int) callconv(.C) void {
    _ = sig;

    while (true) {
        const res = os.waitpid(-1, WNOHANG);
        if (res.pid > 0) {
            continue;
        } else {
            break;
        }
    }
}

pub fn handleRequest(stream: net.Stream) !void {
    var buf: [BUF_SIZE]u8 = undefined;

    while (true) {
        const readBytes = try stream.read(&buf);
        const writeBytes = try stream.write(buf[0..readBytes]);
        if (readBytes != writeBytes) {
            log.err("write() failed", .{});
        }

        if (readBytes <= 0) {
            break;
        }
    }
}

pub fn main() !void {
    try daemons.becomeDaemon(0);

    const sigact = os.Sigaction{
        .handler = .{ .handler = grimReaper },
        .mask = os.empty_sigset,
        .flags = os.SA.RESTART,
    };
    try os.sigaction(os.SIG.CHLD, &sigact, null);

    const host = try net.Address.parseIp("0.0.0.0", 7);
    var server = net.StreamServer.init(net.StreamServer.Options{});
    defer server.deinit();
    try server.listen(host);

    while (true) {
        var connection = try server.accept();
        defer connection.stream.close();

        const pid = try os.fork();
        switch (pid) {
            0 => {
                try handleRequest(connection.stream);
                os.exit(0);
            },
            else => {},
        }
    }
}
