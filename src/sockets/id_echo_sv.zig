const std = @import("std");
const daemons = @import("daemons");
const os = std.os;
const net = std.net;
const log = std.log;

const SERVICE = "echo";
const BUF_SIZE = 500;
const inaddr_any = [4]u8{ 0, 0, 0, 0 };

pub fn main() !void {
    var claddr: net.Address = undefined;
    var buf: [BUF_SIZE]u8 = undefined;
    const sockfd = try os.socket(
        os.AF.INET,
        os.SOCK.DGRAM,
        0,
    );
    try daemons.becomeDaemon(0);

    var svaddr = std.net.Address.initIp4(inaddr_any, 7);
    try os.bind(sockfd, &svaddr.any, svaddr.getOsSockLen());

    while (true) {
        var rev_len = @intCast(os.socklen_t, @sizeOf(os.sockaddr.in));
        var bytesReceived = try os.recvfrom(sockfd, &buf, 0, &claddr.any, &rev_len);

        var bytesSent = try os.sendto(sockfd, buf[0..bytesReceived], 0, &claddr.any, rev_len);
        if (bytesSent != bytesReceived) {
            log.warn("Error echoing response to {s}", .{claddr});
        }
    }
}
