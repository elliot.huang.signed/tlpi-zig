//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                                                                         !
//! Listing 59-4                                                            !
//!                                                                         !
//! i6d_ucase_cl.zig                                                        !
//!                                                                         !
//! Client for i6d_ucase_sv.zig: send each command-line argument as a       !
//! datagram to the server, and then display the contents of the server's   !
//! response datagram.                                                      !
//!                                                                         !
//! See also i6d_ucase_sv.zig.                                              !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const os = std.os;
const mem = std.mem;
const fmt = std.fmt;
const net = std.net;
const log = std.log;
const ascii = std.ascii;
const print = std.debug.print;
const system = os.system;

const BUF_SIZE = 100;

pub fn main() !void {
    const allocator = std.heap.page_allocator;
    const argv = os.argv;
    var buf: [BUF_SIZE]u8 = undefined;

    if (argv.len != 2 or mem.eql(u8, mem.span(argv[1]), "--help")) {
        print("{s} host\n", .{argv[0]});
        return;
    }

    const connection = try net.tcpConnectToHost(allocator, mem.span(argv[1]), 7);
    defer connection.close();

    const pid = try os.fork();
    switch (pid) {
        0 => {
            while (true) {
                const numRead = try connection.read(&buf);
                if (numRead <= 0) {
                    break;
                }
                print("{s}", .{buf[0..numRead]});
            }
            os.exit(0);
        },
        else => {
            while (true) {
                const numRead = try os.read(os.STDIN_FILENO, &buf);
                if (numRead <= 0) {
                    break;
                }

                const numWrite = try connection.write(buf[0..numRead]);
                if (numRead != numWrite) {
                    log.err(
                        "write() failed, numRead: {d}, numWrite: {d}",
                        .{ numRead, numWrite },
                    );
                }
            }
            try os.shutdown(connection.handle, os.ShutdownHow.send);
            os.exit(0);
        },
    }
}
