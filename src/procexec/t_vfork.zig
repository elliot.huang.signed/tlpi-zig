//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                                                                         !
//! Listing 24-4 */                                                         !
//!                                                                         !
//! t_vfork.zig                                                             !
//!                                                                         !
//! Demonstrate the use of vfork() to create a child process.               !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const os = std.os;
const vfork = @import("tlpi").vfork;

pub fn main() !void {
    var istack: usize = 222; // Allocated in stack segment

    var childPid = try vfork();
    switch (childPid) {
        0 => {
            istack *= 3;
            _ = try os.write(os.STDOUT_FILENO, "Child executing\n");
        },
        else => {
            std.time.sleep(3 * 1000000000); // Give child a chance to execute
            _ = try os.write(os.STDOUT_FILENO, "Parent executing\n");
            std.debug.print("istack={d}\n", .{istack});
        },
    }
}
