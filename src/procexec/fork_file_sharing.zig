//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                                                                         !
//! Listing 24-2 */                                                         !
//!                                                                         !
//! fork_file_sharing.zig                                                   !
//!                                                                         !
//! Show that the file descriptors of a forked child refer to the           !
//! same open file objects as the parent.                                   !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const os = std.os;
const tlpi = @import("tlpi");

pub fn main() !void {
    var template = "/tmp/testXXXXXX".*;
    const fd = try tlpi.mkstemp(&template);
    var offset = try os.lseek_CUR_get(fd);
    std.debug.print("File offset before fork(): {d}\n", .{offset});

    var flags = try os.fcntl(fd, os.F.GETFL, 0);
    std.debug.print("O_APPEND flag before fork() is: {s}\n", .{if ((flags & os.O.APPEND) > 0) "on" else "off"});

    const pid = try os.fork();
    switch (pid) {
        0 => {
            try os.lseek_SET(fd, 1000);
            flags = try os.fcntl(fd, os.F.GETFL, 0);
            flags |= os.O.APPEND;
            _ = try os.fcntl(fd, os.F.SETFL, flags);
        },
        else => {
            _ = os.waitpid(-1, 0);
            std.debug.print("Child has exited\n", .{});
            offset = try os.lseek_CUR_get(fd);
            std.debug.print("File offset in parent: {d}\n", .{offset});
            flags = try os.fcntl(fd, os.F.GETFL, 0);
            std.debug.print("O_APPEND flag in parent is: {s}\n", .{if ((flags & os.O.APPEND) > 0) "on" else "off"});
        },
    }
}
