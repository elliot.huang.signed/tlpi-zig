//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                                                                         !
//! Listing 24-6 */                                                         !
//!                                                                         !
//! fork_sig_sync.zig                                                       !
//!                                                                         !
//! Demonstrate how signals can be used to synchronize the actions          !
//! of a parent and child process.                                          !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const tlpi = @import("tlpi");
const time = std.time;
const os = std.os;
const print = std.debug.print;

// Signal handler - does nothing but return
pub fn handler(arg_sig: c_int) callconv(.C) void {
    var sig = arg_sig;
    _ = sig;
}

pub fn main() !void {
    var blockMask = os.empty_sigset;
    var origMask: os.sigset_t = os.empty_sigset;
    var emptyMask: os.sigset_t = os.empty_sigset;
    var sa = os.Sigaction{
        .mask = os.empty_sigset,
        .flags = os.SA.RESTART,
        .handler = .{ .handler = handler },
    };

    os.system.sigaddset(&blockMask, os.SIG.USR1); // Block signal

    os.sigprocmask(os.SIG.BLOCK, &blockMask, &origMask);
    try os.sigaction(os.SIG.USR1, &sa, null);

    const childPid = try os.fork();
    switch (childPid) {
        0 => {
            // Child does some required action here...
            print("[{} {d}] Child started - doing some work\n", .{ time.timestamp(), os.system.getpid() });
            time.sleep(2 * 1000000000); // Simulate time spent doing some work

            print("[{} {d}] Child about to signal parent\n", .{ time.timestamp(), os.system.getpid() });
            try os.kill(tlpi.getppid(), os.SIG.USR1);
            // Now child can do other things...
        },
        else => {
            // Parent may do some work here, and then waits for child to
            // complete the required action
            print("[{} {d}] Parent about to wait for signal\n", .{ time.timestamp(), os.system.getpid() });
            try tlpi.sigsuspendZ(&emptyMask);
            print("[{} {d}] Parent got signal\n", .{ time.timestamp(), os.system.getpid() });

            // If required, return signal mask to its original state
            os.sigprocmask(os.SIG.SETMASK, &origMask, null);

            // Parent carries on to do other things...
        },
    }
}
