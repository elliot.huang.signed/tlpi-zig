//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                                                                         !
//! Listing 24-5 */                                                         !
//!                                                                         !
//! fork_whos_on_first.zig                                                  !
//!                                                                         !
//! Parent repeatedly creates a child, and then processes both race to be   !
//! the first to print a message. (Each child terminates after printing     !
//! its message.) The results of running this program give us an idea of    !
//! which of the two processes--parent or child--is usually scheduled       !
//! first after a fork().                                                   !
//!                                                                         !
//! Whether the child or the parent is scheduled first after fork() has     !
//! changed a number of times across different kernel versions.             !
//!                                                                         !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const os = std.os;
const fmt = std.fmt;

pub fn main() !void {
    var argv = std.os.argv;
    if (argv.len > 1 and std.mem.eql(u8, std.mem.span(argv[1]), "--help")) {
        std.debug.print("{s} [num-children]\n", .{argv[0]});
        return;
    }

    const numChildren = if (argv.len > 1) try fmt.parseInt(usize, std.mem.span(argv[1]), 0) else 1;

    var j: usize = 0;
    while (j < numChildren) : (j += 1) {
        const childPid = try os.fork();
        switch (childPid) {
            0 => {
                std.debug.print("{d} child\n", .{j});
            },
            else => {
                std.debug.print("{d} parent\n", .{j});
                _ = os.waitpid(-1, 0);
            },
        }
    }
}
