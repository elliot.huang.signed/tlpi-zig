//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                                                                         !
//! Listing 24-3 */                                                         !
//!                                                                         !
//! footprint.zig                                                           !
//!                                                                         !
//! Using fork() + wait() to control the memory footprint of an application.!
//!                                                                         !
//! This program contains a function that (artificially) consumes a large   !
//! amount of memory. To avoid changing the process's memory footprint, the !
//! program creates a child process that calls the function. When the child !
//! terminates, all of its memory is freed, and the memory consumption of   !
//! the parent is left unaffected.                                          !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const os = std.os;
const tlpi = @import("tlpi");
var memory: []u16 = undefined;

pub fn func() !void {
    var j: u16 = 0;
    const allocator = std.heap.page_allocator;
    while (j < 0x100) : (j += 1) {
        var addr: usize = undefined;
        memory = try allocator.alloc(u16, 0x8000);
        addr = try tlpi.brk_SET(tlpi.brk_GET() + 0x8000);
    }
    std.debug.print("Program break in child: {d}\n", .{tlpi.brk_GET()});
}

pub fn main() !void {
    std.debug.print("Program break in parent: {d}\n", .{tlpi.brk_GET()});

    var childPid = try os.fork();
    switch (childPid) {
        0 => {
            try func();
        },
        else => {
            const waitResult = os.waitpid(-1, 0);

            std.debug.print("Program break in parent: {d}\n", .{tlpi.brk_GET()});
            std.debug.print("Status = {d} {d}\n", .{ waitResult.status, tlpi.WEXITSTATUS(waitResult.status) });
        },
    }
}
