const std = @import("std");
const os = std.os;
const system = std.os.system;
const mem = std.mem;
const process = std.process;
const print = std.debug.print;
const fmt = std.fmt;
const crypto = std.crypto;
const Sha512 = crypto.hash.sha2.Sha512;

pub const errno = system.getErrno;

const MkstempError = error{
    InvalidTemplate,
} || os.OpenError || os.ClockGetTimeError;

pub fn randname(template: [*]u8) os.ClockGetTimeError!void {
    var ts: system.timespec = undefined;

    try os.clock_gettime(os.CLOCK.REALTIME, &ts);
    var r = ts.tv_sec + ts.tv_nsec + system.gettid() * 65537;

    var i: u8 = 0;
    while (i < 6) : (i += 1) {
        r >>= 5;
        template[i] = 'A' + @intCast(u8, r & 15) + @intCast(u8, (r & 16) * 2);
    }
}

pub fn mkstemp(template: []u8) MkstempError!os.fd_t {
    var len = template.len;
    var ptr: [*]u8 = template.ptr;

    if (len < 6 or !mem.eql(u8, mem.span(@ptrCast(*[6:0]u8, ptr + len - 6)), "XXXXXX")) {
        return MkstempError.InvalidTemplate;
    }

    try randname(ptr + len - 6);
    const fd = try os.open(template, os.O.RDWR | os.O.CREAT | os.O.EXCL, os.S.IRUSR | os.S.IWUSR);
    return fd;
}

pub fn vfork() os.ForkError!os.pid_t {
    const rc = system.vfork();
    switch (errno(rc)) {
        .SUCCESS => return @intCast(os.pid_t, rc),
        .AGAIN => return error.SystemResources,
        .NOMEM => return error.SystemResources,
        else => |err| return os.unexpectedErrno(err),
    }
}

pub fn WEXITSTATUS(s: u32) u32 {
    return (((s) & 0xff00) >> 8);
}

pub fn brk_GET() usize {
    return os.system.syscall1(.brk, 0);
}

pub fn brk_SET(addr: usize) !usize {
    const rc = os.system.syscall1(.brk, addr);
    switch (errno(rc)) {
        .SUCCESS => return rc,
        .NOMEM => return error.OutOfMemory,
        else => |err| return os.unexpectedErrno(err),
    }
}

pub fn sigsuspend(sigmask: *const os.sigset_t) usize {
    return os.system.syscall2(.rt_sigsuspend, @ptrToInt(sigmask), system.NSIG / 8);
}

pub fn sigsuspendZ(sigmask: *const os.sigset_t) !void {
    const rc = sigsuspend(sigmask);
    switch (errno(rc)) {
        .SUCCESS => return,
        .INTR => return,
        else => |err| return os.unexpectedErrno(err),
    }
}

pub fn getppid() os.pid_t {
    return @bitCast(os.pid_t, @truncate(u32, system.syscall0(.getppid)));
}

pub fn clearenv(envmap_ptr: *process.EnvMap) void {
    var iter = envmap_ptr.iterator();

    while (iter.next()) |entry| {
        envmap_ptr.remove(entry.key_ptr.*);
    }
}

pub fn setenv(envmap_ptr: *process.EnvMap, key: []const u8, value: []const u8) !void {
    var res = envmap_ptr.*.get(key);
    if (res) |_| {
        // environment variable exists, do nothing
    } else {
        try envmap_ptr.put(key, value);
    }
}

pub fn unsetenv(envmap_ptr: *process.EnvMap, key: []const u8) void {
    envmap_ptr.remove(key);
}

pub fn putenv(envmap_ptr: *process.EnvMap, env: []const u8) !void {
    var iter = std.mem.split(u8, env, "=");
    var key = iter.first();
    var value = iter.rest();

    try envmap_ptr.put(key, value);
}

pub const pw_name_t = []const u8;
pub const pw_passwd_t = []const u8;
pub const pw_gecos_t = []const u8;
pub const pw_dir_t = []const u8;
pub const pw_shell_t = []const u8;

const passwd = struct {
    pw_name: pw_name_t, // username
    pw_passwd: pw_passwd_t, // user password
    pw_uid: os.uid_t, // user ID
    pw_gid: os.gid_t, // group ID
    pw_gecos: pw_gecos_t, // user information
    pw_dir: pw_dir_t, // home directory
    pw_shell: pw_shell_t, // shell program
};

const GetPwError = error{
    PasswdNotFound,
    EmptyUserName,
};

pub fn getpw(match: anytype) !passwd {
    var buf: [1]u8 = undefined;
    var pw: passwd = undefined;

    if (@TypeOf(match) == pw_name_t) {
        if (match.len == 0) {
            return GetPwError.EmptyUserName;
        }
    }

    var s = std.ArrayList(u8).init(std.heap.page_allocator);
    const fd = try os.open("/etc/passwd", os.O.RDONLY, 0);
    defer os.close(fd);

    while (true) {
        var n = try os.read(fd, &buf);
        try s.append(buf[0]);
        if (n == 0) break;
    }

    var iter = std.mem.split(u8, s.items, "\n");
    while (iter.next()) |entry| {
        var iter_pw = std.mem.split(u8, entry, ":");
        if (iter_pw.next()) |entry_pw| {
            pw.pw_name = entry_pw;
        }
        if (iter_pw.next()) |entry_pw| {
            pw.pw_passwd = entry_pw;
        }
        if (iter_pw.next()) |entry_pw| {
            pw.pw_uid = try fmt.parseInt(u32, entry_pw, 0);
        }
        if (iter_pw.next()) |entry_pw| {
            pw.pw_gid = try fmt.parseInt(u32, entry_pw, 0);
        }
        if (iter_pw.next()) |entry_pw| {
            pw.pw_gecos = entry_pw;
        }
        if (iter_pw.next()) |entry_pw| {
            pw.pw_dir = entry_pw;
        }
        if (iter_pw.next()) |entry_pw| {
            pw.pw_shell = entry_pw;
        }

        if (@TypeOf(match) == pw_name_t) {
            if (mem.eql(u8, pw.pw_name, match)) {
                return pw;
            }
        }

        if (@TypeOf(match) == os.uid_t) {
            if (pw.pw_uid == match) {
                return pw;
            }
        }
    }
    return GetPwError.PasswdNotFound;
}

pub fn getpwnam(name: pw_name_t) !passwd {
    var pw = try getpw(name);
    return pw;
}

pub fn getpwuid(uid: os.uid_t) !passwd {
    var pw = try getpw(uid);
    return pw;
}

pub const gr_name_t = []const u8;
pub const gr_passwd_t = []const u8;
pub const gr_mem_t = std.ArrayList([]const u8);

const group = struct {
    gr_name: gr_name_t,
    gr_passwd: gr_passwd_t,
    gr_gid: os.gid_t,
    gr_mem: gr_mem_t,
};

const GetGrError = error{
    GroupNotFound,
    EmptyGroupName,
};

pub fn getgr(match: anytype) !group {
    var buf: [1]u8 = undefined;
    var gr: group = undefined;

    if (@TypeOf(match) == gr_name_t) {
        if (match.len == 0) {
            return GetPwError.EmptyGroupName;
        }
    }

    var s = std.ArrayList(u8).init(std.heap.page_allocator);
    const fd = try os.open("/etc/group", os.O.RDONLY, 0);
    defer os.close(fd);

    while (true) {
        var n = try os.read(fd, &buf);
        try s.append(buf[0]);
        if (n == 0) break;
    }

    var iter = std.mem.split(u8, s.items, "\n");
    while (iter.next()) |entry| {
        gr.gr_mem = std.ArrayList([]const u8).init(std.heap.page_allocator);
        var iter_gr = std.mem.split(u8, entry, ":");
        if (iter_gr.next()) |entry_gr| {
            gr.gr_name = entry_gr;
        }
        if (iter_gr.next()) |entry_gr| {
            gr.gr_passwd = entry_gr;
        }
        if (iter_gr.next()) |entry_gr| {
            gr.gr_gid = try fmt.parseInt(u32, entry_gr, 0);
        }
        if (iter_gr.next()) |entry_gr| {
            var iter_gr_mem = std.mem.split(u8, entry_gr, ",");
            while (iter_gr_mem.next()) |entry_gr_mem| {
                try gr.gr_mem.append(entry_gr_mem);
            }
        }

        if (@TypeOf(match) == gr_name_t) {
            if (mem.eql(u8, gr.gr_name, match)) {
                return gr;
            }
        }

        if (@TypeOf(match) == os.gid_t) {
            if (gr.gr_gid == match) {
                return gr;
            }
        }
    }
    return GetGrError.GroupNotFound;
}

pub fn getgrnam(name: gr_name_t) !group {
    var gr = try getgr(name);
    return gr;
}

pub fn getgrgid(gid: os.gid_t) !group {
    var gr = try getgr(gid);
    return gr;
}

pub fn userNameFromId(uid: os.uid_t) !pw_name_t {
    var pw = try getpwuid(uid);
    return pw.pw_name;
}

pub fn userIdFromName(name: pw_name_t) !os.uid_t {
    var pw = try getpwnam(name);
    return pw.pw_uid;
}

pub fn groupNameFromId(gid: os.gid_t) !gr_name_t {
    var gr = try getgrgid(gid);
    return gr.gr_name;
}

pub fn groupIdFromName(name: gr_name_t) !os.gid_t {
    var gr = try getgrnam(name);
    return gr.gr_gid;
}

pub const sp_namp_t = []const u8; // Login name (username)
pub const sp_pwdp_t = []const u8; // Encrypted password
pub const sp_lstchg_t = i32; // Time of last password change (days since 1 Jan 1970)
pub const sp_min_t = i32; // Min. number of days between password changes
pub const sp_max_t = i32; // Max. number of days before change required
pub const sp_warn_t = i32; // Number of days beforehand that user is warned of upcoming password expiration
pub const sp_inact_t = i32; // Number of days after expiration that account is considered inactive and locked
pub const sp_expire_t = i32; // Date when account expires (days since 1 Jan 1970)
pub const sp_flag_t = u32; // Reserved for future use

const spwd = struct {
    sp_namp: sp_namp_t,
    sp_pwdp: sp_pwdp_t,
    sp_lstchg: sp_lstchg_t,
    sp_min: sp_min_t,
    sp_max: sp_max_t,
    sp_warn: sp_warn_t,
    sp_inact: sp_inact_t,
    sp_expire: sp_expire_t,
    sp_flag: sp_flag_t,
};

const GetSpError = error{
    SpNotFound,
    EmptyUserName,
};

pub fn getsp(match: anytype) !spwd {
    var buf: [1]u8 = undefined;
    var sp: spwd = undefined;

    if (@TypeOf(match) == sp_namp_t) {
        if (match.len == 0) {
            return GetSpError.EmptyUserName;
        }
    }

    var s = std.ArrayList(u8).init(std.heap.page_allocator);
    const fd = try os.open("/etc/shadow", os.O.RDONLY, 0);
    defer os.close(fd);

    while (true) {
        var n = try os.read(fd, &buf);
        try s.append(buf[0]);
        if (n == 0) break;
    }

    var iter = std.mem.split(u8, s.items, "\n");
    while (iter.next()) |entry| {
        var iter_sp = std.mem.split(u8, entry, ":");
        if (iter_sp.next()) |entry_sp| {
            sp.sp_namp = entry_sp;
        }
        if (iter_sp.next()) |entry_sp| {
            sp.sp_pwdp = entry_sp;
        }
        if (iter_sp.next()) |entry_sp| {
            sp.sp_lstchg = if (entry_sp.len > 0) try fmt.parseInt(i32, entry_sp, 0) else -1;
        }
        if (iter_sp.next()) |entry_sp| {
            sp.sp_min = if (entry_sp.len > 0) try fmt.parseInt(i32, entry_sp, 0) else -1;
        }
        if (iter_sp.next()) |entry_sp| {
            sp.sp_max = if (entry_sp.len > 0) try fmt.parseInt(i32, entry_sp, 0) else -1;
        }
        if (iter_sp.next()) |entry_sp| {
            sp.sp_warn = if (entry_sp.len > 0) try fmt.parseInt(i32, entry_sp, 0) else -1;
        }
        if (iter_sp.next()) |entry_sp| {
            sp.sp_inact = if (entry_sp.len > 0) try fmt.parseInt(i32, entry_sp, 0) else -1;
        }
        if (iter_sp.next()) |entry_sp| {
            sp.sp_expire = if (entry_sp.len > 0) try fmt.parseInt(i32, entry_sp, 0) else -1;
        }
        if (iter_sp.next()) |entry_sp| {
            sp.sp_flag = if (entry_sp.len > 0) try fmt.parseInt(u32, entry_sp, 0) else 0;
        }

        if (@TypeOf(match) == sp_namp_t) {
            if (mem.eql(u8, sp.sp_namp, match)) {
                return sp;
            }
        }
    }
    return GetSpError.SpNotFound;
}

pub fn getspnam(name: sp_namp_t) !spwd {
    var sp = try getsp(name);
    return sp;
}

pub fn tcdrain(fd: os.fd_t) usize {
    return os.system.ioctl(fd, os.system.T.CSBRK, 1);
}

pub const TcdrainError = error{
    FileSystem,
    InterfaceNotFound,
} || os.UnexpectedError;

pub fn tcdrainZ(fd: os.fd_t) TcdrainError!void {
    while (true) {
        const rc = tcdrain(fd);
        switch (errno(rc)) {
            .SUCCESS => return,
            .INVAL => unreachable, // Bad parameters.
            .NOTTY => unreachable,
            .NXIO => unreachable,
            .BADF => unreachable, // Always a race condition.
            .FAULT => unreachable, // Bad pointer parameter.
            .INTR => continue,
            .IO => return error.FileSystem,
            .NODEV => return error.InterfaceNotFound,
            else => |err| return os.unexpectedErrno(err),
        }
    }
}

pub fn getpass(prompt: []const u8) ![]u8 {
    const static = struct {
        var password: [128]u8 = undefined;
    };

    const fd = try os.open("/dev/tty", os.O.RDWR | os.O.NOCTTY | os.O.CLOEXEC, 0);
    defer os.close(fd);
    var t = try os.tcgetattr(fd);
    var s = t;
    t.lflag &= ~(os.system.ECHO | os.system.ISIG);
    t.lflag &= ~(os.system.ECHO | os.system.ISIG);
    t.lflag |= os.system.ICANON;
    t.iflag &= ~(os.system.INLCR | os.system.IGNCR);
    t.iflag |= os.system.ICRNL;
    try os.tcsetattr(fd, os.system.TCSA.FLUSH, t);
    try tcdrainZ(fd);
    print("{s}", .{prompt});
    var len = try os.read(fd, &static.password);
    if (len >= 0) {
        if (len > 0 and static.password[len - 1] == '\n' or len == static.password.len) {
            len -= 1;
        }
    }

    try os.tcsetattr(fd, os.system.TCSA.FLUSH, s);
    print("\n", .{});
    return static.password[0..len];
}

const KEY_MAX = 256;
const SALT_MAX = 16;
const ROUNDS_DEFAULT = 5000;
const ROUNDS_MIN = 1000;
const ROUNDS_MAX = 9999999;

const Sha512CryptError = error{
    InvalidSetting,
    SaltTooLong,
} || fmt.AllocPrintError || fmt.ParseIntError;

const b64 = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".*;

fn to64(s: []u8, u: usize, n: isize) []u8 {
    var uVar = u;
    var nVar = n;
    var sVar = s;
    var i: u8 = 0;
    while (i < nVar) : (i += 1) {
        sVar[0] = b64[uVar % 64];
        sVar.ptr += 1;
        uVar /= 64;
    }
    return sVar;
}

fn hashmd(s: *Sha512, n: usize, md: []const u8) void {
    var i = n;
    while (i > 64) : (i -= 64) {
        s.update(md);
    }
    s.update(md[0..i]);
}

fn sha512Crypt(key: []const u8, setting: []const u8) Sha512CryptError![]u8 {
    var md: [64]u8 = undefined;
    var kmd: [64]u8 = undefined;
    var smd: [64]u8 = undefined;
    var hash: [86]u8 = undefined;
    var slice: []u8 = &hash;
    var i: usize = undefined;

    if (!mem.eql(u8, setting[0..3], "$6$")) {
        return Sha512CryptError.InvalidSetting;
    }

    var salt = setting[3 .. setting.len - 1];
    var r: usize = ROUNDS_DEFAULT;
    if (mem.eql(u8, salt[0.."rounds=".len], "rounds=")) {
        // this is a deviation from the reference:
        // bad rounds setting is rejected if it is
        // - empty
        // - unterminated (missing '$')
        // - begins with anything but a decimal digit
        // the reference implementation treats these bad
        // rounds as part of the salt or parse them with
        // strtoul semantics which may cause problems
        // including non-portable hashes that depend on
        // the host's value of ULONG_MAX.
        var saltIter = mem.split(u8, salt, "$");
        r = try fmt.parseInt(usize, mem.span(saltIter.first()["rounds=".len..]), 0);

        if (r < ROUNDS_MIN) {
            r = ROUNDS_MIN;
        } else if (r > ROUNDS_MAX) {
            r = ROUNDS_MAX;
        }

        salt = saltIter.rest();
    }

    if (salt.len > SALT_MAX) {
        return Sha512CryptError.SaltTooLong;
    }

    var ctx = Sha512.init(.{});
    ctx.update(key);
    ctx.update(salt);
    ctx.update(key);
    ctx.final(md[0..]);

    // A = sha(key salt repeat-B alternate-B-key)
    ctx = Sha512.init(.{});
    ctx.update(key);
    ctx.update(salt);
    hashmd(&ctx, key.len, &md);
    i = key.len;
    while (i > 0) : (i >>= 1) {
        if (i & 1 > 0) {
            ctx.update(&md);
        } else {
            ctx.update(key);
        }
    }
    ctx.final(md[0..]);

    // DP = sha(repeat-key), this step takes O(klen^2) time
    ctx = Sha512.init(.{});
    for (key) |_| {
        ctx.update(key);
    }
    ctx.final(kmd[0..]);

    // DS = sha(repeat-salt)
    ctx = Sha512.init(.{});
    i = 0;
    while (i < @as(usize, 16) + @as(usize, md[0])) : (i += 1) {
        ctx.update(salt);
    }
    ctx.final(smd[0..]);

    // iterate A = f(A,DP,DS), this step takes O(rounds*klen) time
    i = 0;
    while (i < r) : (i += 1) {
        ctx = Sha512.init(.{});
        if (i % 2 > 0) {
            hashmd(&ctx, key.len, &kmd);
        } else {
            ctx.update(&md);
        }

        if (i % 3 > 0) {
            ctx.update(smd[0..salt.len]);
        }

        if (i % 7 > 0) {
            hashmd(&ctx, key.len, &kmd);
        }

        if (i % 2 > 0) {
            ctx.update(&md);
        } else {
            hashmd(&ctx, key.len, &kmd);
        }
        ctx.final(md[0..]);
    }

    const perm = [_][3]u32{
        [_]u32{ 0, 21, 42 },
        [_]u32{ 22, 43, 1 },
        [_]u32{ 44, 2, 23 },
        [_]u32{ 3, 24, 45 },
        [_]u32{ 25, 46, 4 },
        [_]u32{ 47, 5, 26 },
        [_]u32{ 6, 27, 48 },
        [_]u32{ 28, 49, 7 },
        [_]u32{ 50, 8, 29 },
        [_]u32{ 9, 30, 51 },
        [_]u32{ 31, 52, 10 },
        [_]u32{ 53, 11, 32 },
        [_]u32{ 12, 33, 54 },
        [_]u32{ 34, 55, 13 },
        [_]u32{ 56, 14, 35 },
        [_]u32{ 15, 36, 57 },
        [_]u32{ 37, 58, 16 },
        [_]u32{ 59, 17, 38 },
        [_]u32{ 18, 39, 60 },
        [_]u32{ 40, 61, 19 },
        [_]u32{ 62, 20, 41 },
    };

    i = 0;
    while (i < 21) : (i += 1) {
        slice = to64(slice, @intCast(usize, md[perm[i][0]]) << 16 | @intCast(usize, md[perm[i][1]]) << 8 | md[perm[i][2]], 4);
    }
    slice = to64(slice, md[63], 2);
    const output = try fmt.allocPrint(
        std.heap.page_allocator,
        "{s}{s}",
        .{ setting, hash },
    );
    return output;
}

const CryptSha512Error = error{
    SelfTestingFailed,
};

fn cryptSha512(key: []const u8, setting: []const u8) ![]u8 {
    var testKey = "Xy01@#\x01\x02\x80\x7f\xff\r\n\x81\t !".*;
    var testSetting = "$6$rounds=1234$abc0123456789$".*;
    var testHash = "$6$rounds=1234$abc0123456789$BCpt8zLrc/RcyuXmCDOE1ALqMXB2MH6n1g891HhFj8.w7LxGv.FTkqq6Vxc/km3Y0jE0j24jY5PIv/oOu6reg1".*;
    var testOutput = try sha512Crypt(&testKey, &testSetting);
    var output = try sha512Crypt(key, setting);

    if (mem.eql(u8, testOutput, &testHash)) {
        return output;
    } else {
        return CryptSha512Error.SelfTestingFailed;
    }
}

const CryptRError = error{
    UnsupportedSaltFormat,
};

fn cryptR(key: []const u8, salt: []const u8) ![]u8 {
    // ToDo: support more hashing algorithm
    if (salt[0] == '$' and salt.len > 3) {
        if (salt[1] == '6' and salt[2] == '$') {
            var cryptData = try cryptSha512(key, salt);
            return cryptData;
        } else {
            return CryptRError.UnsupportedSaltFormat;
        }
    }
    return CryptRError.UnsupportedSaltFormat;
}

pub fn crypt(key: []const u8, salt: []const u8) ![]u8 {
    var cryptData = try cryptR(key, salt);
    return cryptData;
}

pub fn truncate(path: [*]const u8, length: i64) usize {
    if (@hasField(system.SYS, "truncate64") and system.usize_bits < 64) {
        const length_halves = system.splitValue64(length);
        if (system.require_aligned_register_pair) {
            return system.syscall4(
                .truncate64,
                @ptrToInt(path),
                0,
                length_halves[0],
                length_halves[1],
            );
        } else {
            return system.syscall3(
                .truncate64,
                @ptrToInt(path),
                length_halves[0],
                length_halves[1],
            );
        }
    } else {
        return system.syscall2(
            .truncate,
            @ptrToInt(path),
            @bitCast(usize, length),
        );
    }
}

pub fn truncateZ(file_path: []const u8, length: u64) !void {
    while (true) {
        const ilen = @bitCast(i64, length); // the OS treats this as unsigned
        const file_path_c = try os.toPosixPath(file_path);
        switch (errno(truncate(&file_path_c, ilen))) {
            .SUCCESS => return,
            .INTR => continue,
            .FBIG => return error.FileTooBig,
            .IO => return error.InputOutput,
            .PERM => return error.AccessDenied,
            .TXTBSY => return error.FileBusy,
            .BADF => unreachable, // Handle not open for writing
            .INVAL => unreachable, // Handle not open for writing
            else => |err| return os.unexpectedErrno(err),
        }
    }
}
