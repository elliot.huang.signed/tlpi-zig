# tlpi-zig

The linux programming interface in zig

## BUILD

```
zig build
```

## Description
New to zig and linux system programming? This is all the code exmaples in the book: the linux programming interface but in zig.

## Installation
ToDo

## Usage
All executables are in zig-out/bin, with --help, it will show the command line program's usage.

## Contributing
This is a hobby project of mine, if anyone is interested in contributing, please open a MR or issue.

## Authors and acknowledgment
ToDo

## License
GPL3

## Project status
Experimentatle, more exmaples will be added.
