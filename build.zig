const std = @import("std");
const fs = std.fs;
const mem = std.mem;

fn pkgPath(comptime out: []const u8) std.build.FileSource {
    const outpath = comptime std.fs.path.dirname(@src().file).? ++ std.fs.path.sep_str ++ out;
    return .{ .path = outpath };
}

fn addExe(
    path: []const u8,
    b: *std.build.Builder,
    target: std.zig.CrossTarget,
    mode: std.builtin.Mode,
) void {
    var it = mem.split(u8, fs.path.basename(path), ".");
    const exe = b.addExecutable(it.first(), path);
    exe.setTarget(target);
    exe.setBuildMode(mode);
    exe.install();
    exe.addPackage(.{
        .name = "tlpi",
        .source = pkgPath("lib/tlpi.zig"),
    });
    exe.addPackage(.{
        .name = "daemons",
        .source = pkgPath("src/daemons/daemons.zig"),
    });
}

pub fn build(b: *std.build.Builder) !void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();
    const srcPath = b.option(
        []const u8,
        "srcPath",
        "specify a source to build",
    ) orelse "";

    var fileio = [_][]const u8{
        "src/fileio/copy.zig",
        "src/fileio/seek_io.zig",
        "src/fileio/bad_exclusive_open.zig",
        "src/fileio/t_readv.zig",
        "src/fileio/atomic_append.zig",
        "src/fileio/t_truncate.zig",
        "src/fileio/large_file.zig",
        "src/fileio/multi_descriptors.zig",
    };

    var signals = [_][]const u8{
        "src/signals/ouch.zig",
        "src/signals/intquit.zig",
        "src/signals/t_kill.zig",
        "src/signals/sig_sender.zig",
        "src/signals/sig_receiver.zig",
    };

    var proc = [_][]const u8{
        "src/proc/mem_segments.zig",
        "src/proc/necho.zig",
        "src/proc/display_env.zig",
        "src/proc/modify_env.zig",
    };

    var user_groups = [_][]const u8{
        "src/users_groups/check_password.zig",
    };

    var sysinfo = [_][]const u8{
        "src/sysinfo/procfs_pidmax.zig",
        "src/sysinfo/t_uname.zig",
    };

    var procexec = [_][]const u8{
        "src/procexec/t_fork.zig",
        "src/procexec/fork_file_sharing.zig",
        "src/procexec/footprint.zig",
        "src/procexec/t_vfork.zig",
        "src/procexec/fork_whos_on_first.zig",
        "src/procexec/fork_sig_sync.zig",
    };

    var inotify = [_][]const u8{
        "src/inotify/demo_inotify.zig",
    };

    var threads = [_][]const u8{
        "src/threads/simple_thread.zig",
        "src/threads/thread_incr.zig",
        "src/threads/thread_incr_mutex.zig",
        "src/threads/thread_incr_rwlock.zig",
        "src/threads/prod_no_condvar.zig",
        "src/threads/prod_condvar.zig",
    };

    var daemons = [_][]const u8{
        "src/daemons/test_become_daemon.zig",
    };

    var sockets = [_][]const u8{
        "src/sockets/us_xfr_sv.zig",
        "src/sockets/us_xfr_cl.zig",
        "src/sockets/ud_ucase_sv.zig",
        "src/sockets/ud_ucase_cl.zig",
        "src/sockets/us_abstract_bind.zig",
        "src/sockets/i6d_ucase_sv.zig",
        "src/sockets/i6d_ucase_cl.zig",
        "src/sockets/is_seqnum_sv.zig",
        "src/sockets/is_seqnum_cl.zig",
        "src/sockets/id_echo_sv.zig",
        "src/sockets/id_echo_cl.zig",
        "src/sockets/is_echo_sv.zig",
        "src/sockets/is_echo_cl.zig",
    };

    var altio = [_][]const u8{
        "src/altio/epoll_input.zig",
    };

    var apis = std.ArrayList([][]const u8).init(b.allocator);
    try apis.append(&fileio);
    try apis.append(&signals);
    try apis.append(&proc);
    try apis.append(&user_groups);
    try apis.append(&sysinfo);
    try apis.append(&procexec);
    try apis.append(&inotify);
    try apis.append(&threads);
    try apis.append(&daemons);
    try apis.append(&sockets);
    try apis.append(&altio);

    if (srcPath.len > 0) {
        addExe(srcPath, b, target, mode);
    } else {
        for (apis.items) |api| {
            for (api) |path| {
                addExe(path, b, target, mode);
            }
        }
    }
}
